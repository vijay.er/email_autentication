from django.contrib import admin
from django.urls import path, include

from default import views
from default.views import TokenObtainPairView, TokenRefreshView, TokenPassed

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', TokenPassed.as_view(), name ='hello'),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
