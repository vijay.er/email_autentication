from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenViewBase
from . import serializers


class TokenObtainPairView(TokenViewBase):
    serializer_class = serializers.TokenObtainPairSerializer


token_obtain_pair = TokenObtainPairView.as_view()


class TokenRefreshView(TokenViewBase):
    serializer_class = serializers.TokenRefreshSerializer


token_refresh = TokenRefreshView.as_view()


class TokenPassed(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'message': 'Token Passed'}
        return Response(content)
